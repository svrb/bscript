#!/bin/bash 

#license GPL 3
#https://gitlab.com/svrb/bscript

if [ $# != 1 ]
then
echo usage: $0 transistor_number 
echo Ex:$0 c945
exit
fi
function init_newpart()
{
part=$1
echo init for $part
echo -n "please wait..."
curl -s https://alltransistors.com/search.php\?search\=$part > /tmp/result || exit 1
echo -ne \\r
html2text /tmp/result | grep -i $part | cut -f1 -d ']' | cut -f2 -d'[' | grep -v '!'  > /tmp/list
nl /tmp/list
read -p "enter number " number
address=$(html2text /tmp/result | grep -i $part | cut -f2 -d '(' | tr -d ')' | sed -n ${number}p)
name=$(sed -n ${number}p /tmp/list | tr -d ' ' )
echo -n "please wait..."
curl -s $address > /tmp/$name 
echo -ne \\r

}
function view_description()
{
echo view description 
cat /tmp/$name | tr -d '\n' |  grep -o -P "(?<=\<h1\>$name).*(?=\<div)" | sed 's/<p>/\n/g' | sed 1d
}
function download_datasheet()
{
datasheet_code=$(grep -i "=$name.pdf" /tmp/$name | head -1)
dl_dir=$(echo $datasheet_code | cut -f4 -d'=' | cut -f1 -d'>')
datasheet_name_file=$(echo $datasheet_code | cut -f3 -d'=' | cut -f1 -d'&')
echo downloading datasheet from https://alltransistors.com/pdfdatasheet${dl_dir}/$datasheet_name_file in /tmp/ folder
echo -n "please wait..."
curl -s https://alltransistors.com/pdfdatasheet${dl_dir}/$datasheet_name_file --output /tmp/$datasheet_name_file
echo -ne \\r
}


function download_datasheet_from_list()
{
iconv -c -t utf-8 /tmp/$name  | html2text --protect-links -b 0 | grep -n 'doc.*\.pdf' | grep -vi \\.jpg | grep -o -P "(?<=\(\<).*(?=\>\))" > /tmp/datasheet_list_link
grep -o -P "(?<=doc\=).*(?=\&)" /tmp/datasheet_list_link | nl
read -p "which one you want to download it? " link 
dl_dir=$(sed -n ${link}p /tmp/datasheet_list_link | grep -o -P "(?<=dire\=).*(?=\$)" )
datasheet_name_file=$(sed -n ${link}p /tmp/datasheet_list_link | grep -o -P  "(?<=doc\=).*(?=\&)")
echo downloading datasheet from https://alltransistors.com/pdfdatasheet${dl_dir}/$datasheet_name_file in /tmp/ folder
echo -n "please wait..."
curl -s https://alltransistors.com/pdfdatasheet${dl_dir}/$datasheet_name_file --output /tmp/$datasheet_name_file
echo -ne \\r
}

function cross_search()
{
echo -n "please wait..."
curl -s $(grep crsearch.php /tmp/$name | cut -f3- -d '=') --output /tmp/crosssearch
echo -ne \\r

cross_table_start=$(sed -n '/<table class="sort"><thead>/=' /tmp/crosssearch)
cross_table_stop=$(sed -n '/<\/td><\/tr><\/tbody><\/table><p>/=' /tmp/crosssearch)
sed -n $cross_table_start,${cross_table_stop}p /tmp/crosssearch | html2text -b 0 | sed -e 's/\((\).*\()\)/\1\2/' | sed 's/()|/|/g' | sed 's/]|/]/g'
}
echo '
d(escription) # summary feature about transistor
ds # DataSheet of file. it search for matching only part number
dsl # DataSheet List. it show all datasheet available.
c(ross search) # search for similar transistor
q(uit) # exit!
any other string use for init new part #you must init new part for change part.'

init_newpart $1
while true
do
read -p "please enter your command (current transistor:$name) : "  command
case $command in

q)
exit ;;

d)
view_description ;;

ds)
download_datasheet ;;

dsl)
download_datasheet_from_list ;;

c)cross_search ;;


*)
init_newpart $command ;;
esac
done
