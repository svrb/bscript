#!/bin/bash 

#license GPL 3
#https://gitlab.com/svrb/bscript

tol=$1  
arz=$2
ertefa=$3
tickness=$4
if [ $# != 4 ] 
then 
read -p "tol (mm): " tol 
read -p "arz (mm): " arz 
read -p "ertefa (mm): " ertefa 
read -p "zekhamat (mm): " tickness 
fi



tab_tol_start=$(( tol /4 ))
tab_tol_stop=$((tol -  tab_tol_start))

tab_arz_start=$(( arz /4 ))
tab_arz_stop=$((arz - tab_arz_start ))

tab_ertefa_start=$((ertefa / 4))
tab_ertefa_stop=$((ertefa - tab_ertefa_start )) 

pice_top_tol=$tol 
pice_top_arz=$arz 
pice_top_tol_tab_start=$tab_tol_start
pice_top_tol_tab_stop=$tab_tol_stop 
pice_top_arz_tab_start=$tab_arz_start
pice_top_arz_tab_stop=$tab_arz_stop

pice_side_tol=$tol 
pice_side_arz=$ertefa 
pice_side_tol_tab_start=$tab_tol_start
pice_side_tol_tab_stop=$tab_tol_stop 
pice_side_arz_tab_start=$tab_ertefa_start
pice_side_arz_tab_stop=$tab_ertefa_stop

pice_front_tol=$arz
pice_front_arz=$ertefa
pice_front_tol_tab_start=$tab_arz_start
pice_front_tol_tab_stop=$tab_arz_stop 
pice_front_arz_tab_start=$tab_ertefa_start
pice_front_arz_tab_stop=$tab_ertefa_stop

marginy_row1=0
marginy_row2=$((pice_top_arz + 2 * tickness ))
marginx_column1=0 
marginx_column2=$((pice_top_tol + 2 * tickness))
marginx_column3=$((2 * marginx_column2 ))

echo '<?xml version="1.0" encoding="UTF-8" standalone="no"?>

<svg>' > /tmp/my.svg

echo -n '
    <polyline
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       points="' >> /tmp/my.svg

marginx=$marginx_column1
marginy=$marginy_row1
#top 
{       
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( marginy + 0 ))
echo -n $(( marginx+tab_tol_start )) ; echo -n ',' ; echo  $(( marginy + 0 ))
echo -n $(( marginx+tab_tol_start )) ; echo -n ',' ; echo  $(( marginy - tickness ))
echo -n $(( marginx + tab_tol_stop )) ; echo -n ',' ; echo  $(( marginy - tickness ))
echo -n $(( marginx + tab_tol_stop )) ; echo -n ',' ; echo  $(( marginy + 0 ))
echo -n $(( marginx + tol + tickness )) ; echo -n ',' ; echo  $(( marginy + 0 ))
echo -n $(( marginx + tol + tickness )) ; echo -n ',' ; echo  $(( pice_top_arz_tab_start + marginy ))
echo -n $(( marginx + tol )) ; echo -n ',' ; echo  $(( pice_top_arz_tab_start+ marginy ))
echo -n $(( marginx + tol )) ; echo -n ',' ; echo  $(( pice_top_arz_tab_stop + marginy ))
echo -n $(( marginx + tol + tickness )) ; echo -n ',' ; echo  $(( pice_top_arz_tab_stop + marginy ))
echo -n $(( marginx + tol + tickness )) ; echo -n ',' ; echo  $(( arz + marginy ))
echo -n $(( marginx + tol - pice_top_tol_tab_start )) ; echo -n ',' ; echo  $(( arz + marginy ))
echo -n $(( marginx + tol - pice_top_tol_tab_start )) ; echo -n ',' ; echo  $(( arz + tickness + marginy ))
echo -n $(( marginx + tol - pice_top_tol_tab_stop )) ; echo -n ',' ; echo  $(( arz + tickness + marginy ))
echo -n $(( marginx + tol - pice_top_tol_tab_stop )) ; echo -n ',' ; echo  $(( arz + marginy ))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( arz+ marginy ))
echo -n $(( marginx - tickness )) ; echo -n ',' ; echo  $(( arz + marginy ))
echo -n $(( marginx - tickness )) ; echo -n ',' ; echo  $(( arz - pice_top_arz_tab_start + marginy ))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( arz - pice_top_arz_tab_start + marginy ))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( arz - pice_top_arz_tab_stop + marginy ))
echo -n $(( marginx -tickness )) ; echo -n ',' ; echo  $(( arz - pice_top_arz_tab_stop + marginy ))
echo -n $(( marginx - tickness )) ; echo -n ',' ; echo  $(( 0 + marginy ))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( 0 + marginy ))
} | tr '\n' ' ' >> /tmp/my.svg 
echo '"
       id="top"
        />
' >> /tmp/my.svg # end item.





echo -n '
    <polyline
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       points="' >> /tmp/my.svg
       
marginx=$marginx_column2
marginy=$marginy_row1
#botom 
{       
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( marginy + 0 ))
echo -n $(( marginx+tab_tol_start )) ; echo -n ',' ; echo  $(( marginy + 0 ))
echo -n $(( marginx+tab_tol_start )) ; echo -n ',' ; echo  $(( marginy - tickness ))
echo -n $(( marginx + tab_tol_stop )) ; echo -n ',' ; echo  $(( marginy - tickness ))
echo -n $(( marginx + tab_tol_stop )) ; echo -n ',' ; echo  $(( marginy + 0 ))
echo -n $(( marginx + tol + tickness )) ; echo -n ',' ; echo  $(( marginy + 0 ))
echo -n $(( marginx + tol + tickness )) ; echo -n ',' ; echo  $(( pice_top_arz_tab_start + marginy ))
echo -n $(( marginx + tol )) ; echo -n ',' ; echo  $(( pice_top_arz_tab_start+ marginy ))
echo -n $(( marginx + tol )) ; echo -n ',' ; echo  $(( pice_top_arz_tab_stop + marginy ))
echo -n $(( marginx + tol + tickness )) ; echo -n ',' ; echo  $(( pice_top_arz_tab_stop + marginy ))
echo -n $(( marginx + tol + tickness )) ; echo -n ',' ; echo  $(( arz + marginy ))
echo -n $(( marginx + tol - pice_top_tol_tab_start )) ; echo -n ',' ; echo  $(( arz + marginy ))
echo -n $(( marginx + tol - pice_top_tol_tab_start )) ; echo -n ',' ; echo  $(( arz + tickness + marginy ))
echo -n $(( marginx + tol - pice_top_tol_tab_stop )) ; echo -n ',' ; echo  $(( arz + tickness + marginy ))
echo -n $(( marginx + tol - pice_top_tol_tab_stop )) ; echo -n ',' ; echo  $(( arz + marginy ))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( arz+ marginy ))
echo -n $(( marginx - tickness )) ; echo -n ',' ; echo  $(( arz + marginy ))
echo -n $(( marginx - tickness )) ; echo -n ',' ; echo  $(( arz - pice_top_arz_tab_start + marginy ))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( arz - pice_top_arz_tab_start + marginy ))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( arz - pice_top_arz_tab_stop + marginy ))
echo -n $(( marginx -tickness )) ; echo -n ',' ; echo  $(( arz - pice_top_arz_tab_stop + marginy ))
echo -n $(( marginx - tickness )) ; echo -n ',' ; echo  $(( 0 + marginy ))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( 0 + marginy ))
} | tr '\n' ' ' >> /tmp/my.svg 
echo '"
       id="bottom"
        />
' >> /tmp/my.svg # end item.

echo -n '<polyline
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       points="' >> /tmp/my.svg
       
marginx=$marginx_column1
marginy=$marginy_row2
#side left
{
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( 0 + marginy))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( -1 * tickness+ marginy))
echo -n $(( marginx+ pice_side_tol_tab_start )) ; echo -n ',' ; echo  $(( -1 * tickness + marginy))
echo -n $(( marginx+ pice_side_tol_tab_start )) ; echo -n ',' ; echo  $(( 0 + marginy))
echo -n $(( marginx + pice_side_tol_tab_stop )) ; echo -n ',' ; echo  $(( 0 + marginy))
echo -n $(( marginx + pice_side_tol_tab_stop )) ; echo -n ',' ; echo  $(( -1 * tickness + marginy))
echo -n $(( marginx + pice_side_tol + tickness )) ; echo -n ',' ; echo  $(( -1 * tickness + marginy))
echo -n $(( marginx + pice_side_tol + tickness )) ; echo -n ',' ; echo  $(( pice_side_arz_tab_start + marginy))
echo -n $(( marginx + pice_side_tol )) ; echo -n ',' ; echo  $(( pice_side_arz_tab_start+ marginy))
echo -n $(( marginx + pice_side_tol )) ; echo -n ',' ; echo  $(( pice_side_arz_tab_stop + marginy))
echo -n $(( marginx + pice_side_tol + tickness )) ; echo -n ',' ; echo  $(( pice_side_arz_tab_stop + marginy))
echo -n $(( marginx + pice_side_tol + tickness )) ; echo -n ',' ; echo  $(( pice_side_arz + marginy))
echo -n $(( marginx + pice_side_tol + tickness )) ; echo -n ',' ; echo  $(( pice_side_arz + tickness + marginy))
echo -n $(( marginx + pice_side_tol - pice_side_tol_tab_start )) ; echo -n ',' ; echo  $(( pice_side_arz + tickness+ marginy))
echo -n $(( marginx + pice_side_tol - pice_side_tol_tab_start )) ; echo -n ',' ; echo  $(( pice_side_arz + marginy))
echo -n $(( marginx + pice_side_tol_tab_start )) ; echo -n ',' ; echo  $(( pice_side_arz+ marginy))
echo -n $(( marginx + pice_side_tol_tab_start )) ; echo -n ',' ; echo  $(( pice_side_arz + tickness+ marginy))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( pice_side_arz + tickness + marginy))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( pice_side_arz + marginy))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( pice_side_arz - pice_side_arz_tab_start + marginy))
echo -n $(( marginx - tickness )) ; echo -n ',' ; echo  $(( pice_side_arz - pice_side_arz_tab_start + marginy))
echo -n $(( marginx - tickness )) ; echo -n ',' ; echo  $(( pice_side_arz - pice_side_arz_tab_stop + marginy))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( pice_side_arz_tab_start + marginy))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( 0 + marginy))
} | tr '\n' ' ' >>/tmp/my.svg

echo '"
       id="left"
        />' >>/tmp/my.svg #end left item
        
        
echo -n '<polyline
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       points="' >> /tmp/my.svg
       
marginx=$marginx_column2
marginy=$marginy_row2
#side right
{
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( 0 + marginy))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( -1 * tickness+ marginy))
echo -n $(( marginx+ pice_side_tol_tab_start )) ; echo -n ',' ; echo  $(( -1 * tickness + marginy))
echo -n $(( marginx+ pice_side_tol_tab_start )) ; echo -n ',' ; echo  $(( 0 + marginy))
echo -n $(( marginx + pice_side_tol_tab_stop )) ; echo -n ',' ; echo  $(( 0 + marginy))
echo -n $(( marginx + pice_side_tol_tab_stop )) ; echo -n ',' ; echo  $(( -1 * tickness + marginy))
echo -n $(( marginx + pice_side_tol + tickness )) ; echo -n ',' ; echo  $(( -1 * tickness + marginy))
echo -n $(( marginx + pice_side_tol + tickness )) ; echo -n ',' ; echo  $(( pice_side_arz_tab_start + marginy))
echo -n $(( marginx + pice_side_tol )) ; echo -n ',' ; echo  $(( pice_side_arz_tab_start+ marginy))
echo -n $(( marginx + pice_side_tol )) ; echo -n ',' ; echo  $(( pice_side_arz_tab_stop + marginy))
echo -n $(( marginx + pice_side_tol + tickness )) ; echo -n ',' ; echo  $(( pice_side_arz_tab_stop + marginy))
echo -n $(( marginx + pice_side_tol + tickness )) ; echo -n ',' ; echo  $(( pice_side_arz + marginy))
echo -n $(( marginx + pice_side_tol + tickness )) ; echo -n ',' ; echo  $(( pice_side_arz + tickness + marginy))
echo -n $(( marginx + pice_side_tol - pice_side_tol_tab_start )) ; echo -n ',' ; echo  $(( pice_side_arz + tickness+ marginy))
echo -n $(( marginx + pice_side_tol - pice_side_tol_tab_start )) ; echo -n ',' ; echo  $(( pice_side_arz + marginy))
echo -n $(( marginx + pice_side_tol_tab_start )) ; echo -n ',' ; echo  $(( pice_side_arz+ marginy))
echo -n $(( marginx + pice_side_tol_tab_start )) ; echo -n ',' ; echo  $(( pice_side_arz + tickness+ marginy))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( pice_side_arz + tickness + marginy))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( pice_side_arz + marginy))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( pice_side_arz - pice_side_arz_tab_start + marginy))
echo -n $(( marginx - tickness )) ; echo -n ',' ; echo  $(( pice_side_arz - pice_side_arz_tab_start + marginy))
echo -n $(( marginx - tickness )) ; echo -n ',' ; echo  $(( pice_side_arz - pice_side_arz_tab_stop + marginy))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( pice_side_arz_tab_start + marginy))
echo -n $(( marginx )) ; echo -n ',' ; echo  $(( 0 + marginy))
} | tr '\n' ' ' >>/tmp/my.svg

echo '"
       id="right"
        />' >>/tmp/my.svg #end right item
        
        
echo -n '<polyline
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       points="' >> /tmp/my.svg
       
marginx=$marginx_column3
marginy=$marginy_row1
#front 
{
echo -n $((marginx)) ; echo -n ',' ; echo $(( 0 + marginy ))
echo -n $((marginx+ pice_front_tol_tab_start)) ; echo -n ',' ; echo $(( 0 + marginy ))
echo -n $((marginx+pice_front_tol_tab_start)) ; echo -n ',' ; echo $(( -1 * tickness + marginy ))
echo -n $((marginx + pice_front_tol_tab_stop)) ; echo -n ',' ; echo $(( -1 * tickness + marginy ))
echo -n $((marginx + pice_front_tol_tab_stop)) ; echo -n ',' ; echo $(( 0 + marginy ))
echo -n $((marginx + pice_front_tol + tickness)) ; echo -n ',' ; echo $(( 0 + marginy ))
echo -n $((marginx + pice_front_tol + tickness)) ; echo -n ',' ; echo $(( pice_front_arz_tab_start + marginy ))
echo -n $((marginx + pice_front_tol)) ; echo -n ',' ; echo $(( pice_front_arz_tab_start+ marginy ))
echo -n $((marginx + pice_front_tol)) ; echo -n ',' ; echo $(( pice_front_arz_tab_stop + marginy ))
echo -n $(( marginx + pice_front_tol + tickness)) ; echo -n ',' ; echo $(( pice_front_arz_tab_stop + marginy ))
echo -n $(( marginx + pice_front_tol + tickness)) ; echo -n ',' ; echo $(( pice_front_arz + marginy ))
echo -n $(( marginx + pice_front_tol + tickness)) ; echo -n ',' ; echo $(( pice_front_arz + marginy ))
echo -n $((marginx + pice_front_tol - pice_front_tol_tab_start)) ; echo -n ',' ; echo $(( pice_front_arz + marginy ))
echo -n $((marginx + pice_front_tol - pice_front_tol_tab_start)) ; echo -n ',' ; echo $(( pice_front_arz + tickness + marginy ))
echo -n $((marginx + pice_front_tol_tab_start)) ; echo -n ',' ; echo $(( pice_front_arz + tickness + marginy ))
echo -n $((marginx + pice_front_tol_tab_start)) ; echo -n ',' ; echo $(( pice_front_arz + marginy ))
echo -n $((marginx)) ; echo -n ',' ; echo $(( pice_front_arz + marginy ))
echo -n $((marginx)) ; echo -n ',' ; echo $(( pice_front_arz + marginy ))
echo -n $(( marginx)) ; echo -n ',' ; echo $(( pice_front_arz - pice_front_arz_tab_start + marginy ))
echo -n $(( marginx - tickness)) ; echo -n ',' ; echo $(( pice_front_arz - pice_front_arz_tab_start + marginy ))
echo -n $(( marginx - tickness)) ; echo -n ',' ; echo $(( pice_front_arz - pice_front_arz_tab_stop + marginy ))
echo -n $(( marginx)) ; echo -n ',' ; echo $(( pice_front_arz_tab_start + marginy ))
echo -n $(( marginx)) ; echo -n ',' ; echo $(( 0 + marginy ))
} | tr '\n' ' ' >>/tmp/my.svg
echo '"
       id="front"
        />' >>/tmp/my.svg #end front 

echo -n '<polyline
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       points="' >> /tmp/my.svg
       
marginx=$marginx_column3
marginy=$marginy_row2
#front 
{
echo -n $((marginx)) ; echo -n ',' ; echo $(( 0 + marginy ))
echo -n $((marginx+ pice_front_tol_tab_start)) ; echo -n ',' ; echo $(( 0 + marginy ))
echo -n $((marginx+pice_front_tol_tab_start)) ; echo -n ',' ; echo $(( -1 * tickness + marginy ))
echo -n $((marginx + pice_front_tol_tab_stop)) ; echo -n ',' ; echo $(( -1 * tickness + marginy ))
echo -n $((marginx + pice_front_tol_tab_stop)) ; echo -n ',' ; echo $(( 0 + marginy ))
echo -n $((marginx + pice_front_tol + tickness)) ; echo -n ',' ; echo $(( 0 + marginy ))
echo -n $((marginx + pice_front_tol + tickness)) ; echo -n ',' ; echo $(( pice_front_arz_tab_start + marginy ))
echo -n $((marginx + pice_front_tol)) ; echo -n ',' ; echo $(( pice_front_arz_tab_start+ marginy ))
echo -n $((marginx + pice_front_tol)) ; echo -n ',' ; echo $(( pice_front_arz_tab_stop + marginy ))
echo -n $(( marginx + pice_front_tol + tickness)) ; echo -n ',' ; echo $(( pice_front_arz_tab_stop + marginy ))
echo -n $(( marginx + pice_front_tol + tickness)) ; echo -n ',' ; echo $(( pice_front_arz + marginy ))
echo -n $(( marginx + pice_front_tol + tickness)) ; echo -n ',' ; echo $(( pice_front_arz + marginy ))
echo -n $((marginx + pice_front_tol - pice_front_tol_tab_start)) ; echo -n ',' ; echo $(( pice_front_arz + marginy ))
echo -n $((marginx + pice_front_tol - pice_front_tol_tab_start)) ; echo -n ',' ; echo $(( pice_front_arz + tickness + marginy ))
echo -n $((marginx + pice_front_tol_tab_start)) ; echo -n ',' ; echo $(( pice_front_arz + tickness + marginy ))
echo -n $((marginx + pice_front_tol_tab_start)) ; echo -n ',' ; echo $(( pice_front_arz + marginy ))
echo -n $((marginx)) ; echo -n ',' ; echo $(( pice_front_arz + marginy ))
echo -n $((marginx)) ; echo -n ',' ; echo $(( pice_front_arz + marginy ))
echo -n $(( marginx)) ; echo -n ',' ; echo $(( pice_front_arz - pice_front_arz_tab_start + marginy ))
echo -n $(( marginx - tickness)) ; echo -n ',' ; echo $(( pice_front_arz - pice_front_arz_tab_start + marginy ))
echo -n $(( marginx - tickness)) ; echo -n ',' ; echo $(( pice_front_arz - pice_front_arz_tab_stop + marginy ))
echo -n $(( marginx)) ; echo -n ',' ; echo $(( pice_front_arz_tab_start + marginy ))
echo -n $(( marginx)) ; echo -n ',' ; echo $(( 0 + marginy ))
} | tr '\n' ' ' >>/tmp/my.svg
echo '"
       id="back"
        />' >>/tmp/my.svg #end back
echo "</svg>" >>/tmp/my.svg # end file 

echo you must resize your plan to $((tol + tol + arz + 6 * zekhamat )) x $(( arz + zekhamat + 4 * zekhamat )) mm
echo your plan save in /tmp/my.svg! 
