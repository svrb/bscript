 #!/bin/bash
 
 if [ $# != '1' ] 
 then
 echo usage $0 part_name
 echo if product found its price will be displayed.
 exit
 fi
 part=$(echo $1 | tr ' ' '+')
 
curl "https://eshop.eca.ir/%D8%AC%D8%B3%D8%AA%D8%AC%D9%88?controller=search&orderby=position&orderway=desc&search_query=$part&n=100&submit_search=" | sed 's/<\/div>/\n/g' | grep "product-name" | html2text -b 0 --ignore-links
