#!/bin/bash

#license: GPL 3
#https://gitlab.com/svrb/bscript

cd /tmp/
part=$1
echo -n "please wait "
page1=`curl -s http://www.datasheet-pdf.com/Search-Datasheet-PDF/$1 | grep pdf.gif | head -1 | cut -f2 -d "'"`
echo -n 30%
page2=$(curl -s http://www.datasheet-pdf.com$page1 | grep -o -P "(?<=ifram).*(?=\/ifram)" | grep src | grep .pdf | cut -f4 -d"'")
echo -n  60%
pdffile=$(curl -s http://www.datasheet-pdf.com$page2 | grep -o -P "(?<=ifram).*(?=\/ifram)" | cut -f4 -d"'")
echo -n 90%
wget $pdffile
echo -n 100%
echo all done. bye